#!/bin/bash

# for j in 22 21 20 19 18 ;do
for j in 19 18 17 16 15 14 13 12  ;do
	echo "experimenting size " $j
	# dd of=/dev/xdevcfg if=../Bitstreams/Gen_matrix_matrix/Gen_matrix_matrix_${j}.bit 2> /dev/null
	dd of=/dev/xdevcfg if=../Bitstreams/Gen_matrix_vector_10/Gen_matrix_vector_10_${j}.bit 2> /dev/null
	for i in {1..5};do
		./run.py $j
	done
done
