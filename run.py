#!/usr/bin/python
from lift import LiftDevice
from submit import *
import submit
import numpy as np
import time
import timeit

device = LiftDevice()

def run_id(size=2**22):
	# prepare
	# size = 2**22
	a = device.malloc(size, np.int64)
	a[:] = np.random.randint(255, size=size/8)
	
	# run
	t0 = time.time()
	b, interrupt = submit_id(device, a)
	t1 = time.time()

	# print interrupt
	# evaluate
	# print "correct: ", (a==b).all()

	throu = a.nbytes / ((t1-t0)*1024*1024)
	# print "throughput: ", throu, "MB/s"
	print (t1-t0)*1000
	


def run_sum(size=64):
	a = device.malloc(size, np.int64)
	a[:] = np.random.randint(255, size=size/8)

	# run
	t = timeit.timeit(lambda: submit.submit_sum(device, a), number=1)
	print t*1000

	# t = timeit.timeit(lambda: np.sum(a), number=1)
	# print t*1000

	# a = np.random.randint(255, size=size/8).astype(np.int64)
	
	# t = timeit.timeit(lambda:np.sum(a), number=1)
	# print t*1000


def run_dot(size=2**13):
	import subprocess
	import os
	from subprocess import check_call

	a = device.malloc(size, np.int64)
	b = device.malloc(size, np.int64)
	a[:] = np.random.randint(10, size=a.shape)
	b[:] = np.random.randint(10, size=a.shape)

	# with open(os.devnull, 'w') as devnull:
	# 	check_call(["dd", "of=/dev/xdevcfg", "if=../Bitstreams/Gen_dot/Gen_dot_{}.bit".format(size/8)],
	# 						stdout=devnull, stderr=devnull)
	# t = timeit.timeit(lambda: submit.submit_dot(device, a, b), number=1)
	# print t*1000

	# t = timeit.timeit(lambda:np.dot(a,b), number=1)
	# print t*1000

	a = np.random.randint(10, size=a.shape).astype(np.int64)
	b = np.random.randint(10, size=a.shape).astype(np.int64)
	t = timeit.timeit(lambda:np.dot(a,b), number=1)
	print t*1000

def run_vec(col=15):
	row = 19-col
	a = device.malloc(8*(2**(row+col)), np.int64)
	a.shape = (2**row, 2**col)
	b = device.malloc(8*(2**col), np.int64)

	a[:] = np.random.randint(10, size=a.shape)
	b[:] = np.random.randint(10, size=b.shape)

	
	# t = timeit.timeit(lambda: submit_vec(device,a,b), number=1)
	# print t*1000

	# t = timeit.timeit(lambda: np.dot(a,b), number=1)
	# print t*1000

	a = np.random.randint(10, size=a.shape).astype(np.int64)
	b = np.random.randint(10, size=b.shape).astype(np.int64)
	t = timeit.timeit(lambda: np.dot(a,b), number=1)
	print t*1000

def run_vec_14(total=19):
	col = 14
	row = total-col
	a = device.malloc(8*(2**(row+col)), np.int64)
	a.shape = (2**row, 2**col)
	b = device.malloc(8*(2**col), np.int64)

	a[:] = np.random.randint(10, size=a.shape)
	b[:] = np.random.randint(10, size=b.shape)

	
	# t = timeit.timeit(lambda: submit_vec(device,a,b), number=1)
	# print t*1000

	# t = timeit.timeit(lambda: np.dot(a,b), number=1)
	# print t*1000

	a = np.random.randint(10, size=a.shape).astype(np.int64)
	b = np.random.randint(10, size=b.shape).astype(np.int64)
	t = timeit.timeit(lambda: np.dot(a,b), number=1)
	print t*1000

def run_vec_10(total=19):
	col = 10
	row = total-col
	a = device.malloc(8*(2**(row+col)), np.int64)
	a.shape = (2**row, 2**col)
	b = device.malloc(8*(2**col), np.int64)

	a[:] = np.random.randint(10, size=a.shape)
	b[:] = np.random.randint(10, size=b.shape)

	
	t = timeit.timeit(lambda: submit_vec(device,a,b), number=1)
	
	print t*1000
	# c = submit_vec(device, a, b)
	# print (c==np.dot(a,b)).all()

	# t = timeit.timeit(lambda: np.dot(a,b), number=1)
	# print t*1000


	# a = np.random.randint(10, size=a.shape).astype(np.int64)
	# b = np.random.randint(10, size=b.shape).astype(np.int64)
	# t = timeit.timeit(lambda: np.dot(a,b), number=1)
	# print t*1000

def run_matrix(total=19):
	col = 7
	row = total-col
	b = device.malloc(8*(2**(col+col)), np.int64)
	b.shape = (2**col, 2**col)
	a = device.malloc(8*(2**(col+row)), np.int64)
	a.shape = (2**row, 2**col)

	a[:] = np.random.randint(10, size=a.shape)
	b[:] = np.random.randint(10, size=b.shape)

	
	t = timeit.timeit(lambda: submit_mat(device, a, b), number=1)
	
	print t*1000

	# a = np.random.randint(10, size=a.shape).astype(np.int64)
	# b = np.random.randint(10, size=b.shape).astype(np.int64)
	# # print (c==np.dot(a,b)).all()
	# t = timeit.timeit(lambda: np.dot(a,b), number=1)
	# print t*1000


import sys
if __name__ == "__main__" or False:
	# run_id(2**int(sys.argv[1]))
	# run_sum(2**int(sys.argv[1]))
	# run_dot(2**int(sys.argv[1]))
	# run_vec_14(int(sys.argv[1]))
	# run_matrix(int(sys.argv[1]))
	run_vec_10(int(sys.argv[1]))
