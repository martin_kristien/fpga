
import numpy as np
import time

def submit_id(device, a):
	# type check
	a_base, a_size = device.mem_uio.phy_buf(a)
	# device.st
	# device.dma._dma_uio.enable_interrupts()
	# assert a_base != None
	# assert a_size <= 2**22 and 2**3 <= a_size, "size is {}".format(a_size)
	# assert a.nbytes % 8 == 0, "nbytes must be divisible by 8, remainder: {}".format(a.nbytes%8)

	# allocation
	# source = a
	dest = device.malloc(a.nbytes, a.dtype)
	
	# launch stream
	device.toHost(dest)
	# device.waitForInterrupt()
	device.toFPGA(a)
	# device.st
	i = device.waitForInterrupt()
	# i = device.wait()
	# device.st
	return dest, i

def submit_sum(device, p72):

	# allocate memory for result and read from device
	t0 = device.malloc(8, np.int64)
	device.toHost(t0)

	# write data to device
	device.toFPGA(p72)

	# i = device.waitForIdle()
	i = device.waitForInterrupt()
	# device.st
	return t0

def submit_dot(device, a, b):
	t0 = device.malloc(8, np.int64)
	device.toHost(t0)

	device.toFPGA(a)
	device.waitForToFPGA()
	# device.waitForInterrupt()
	# device.st

	device.toFPGA(b)
	# device.waitForIdle()
	device.waitForInterrupt()
	# device.st

def submit_vec(device, a, b):
	t0 = device.malloc(8*a.shape[0], np.int64)
	device.toHost(t0)

	device.toFPGA(b)
	device.waitForToFPGA()
	# device.st
	device.toFPGA(a)
	device.waitForInterrupt()
	# device.st
	return t0

def submit_mat(device, a, b):
	# allocate memory for result and read from device
	t0 = device.malloc(a.nbytes, np.int64)
	device.toHost(t0)

	# transpose
	temp_1 = device.malloc(b.nbytes, b.dtype)
	temp_1.shape = (b.shape[1], b.shape[0]) + b.shape[2:]
	temp_1[:] = np.transpose(b,[1,0] + range(2, b.ndim))

	# temp_1 = b
	# flatten
	# write data to device
	device.toFPGA(temp_1)
	device.waitForToFPGA()
	# flatten
	# write data to device
	device.toFPGA(a)
	t0.shape = a.shape
	device.waitForInterrupt()
	return t0
