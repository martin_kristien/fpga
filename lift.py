#!/usr/bin/python
import numpy as np
import time
# sys.settrace

class LiftDevice:
	def __init__(self):
		from axidma import AxiDMA
		import uio
		# import numpy as np
		
		self.dma = AxiDMA('dma')					# type: AxiDMA
		# self.dma._dma_uio.enable_interrupts()
		# self.dma._dma_uio.enable_interrupts()
		self.dma.reset()
		self.dma.enable_interrupts()
		self.dma._dma_uio.enable_interrupts()
		self.mem_uio = uio.UIO('scratch_mem')		# type: UIO
		self.mem = self.mem_uio.as_ndarray(dtype=np.uint8)
		self.phy_base, _ = self.mem_uio.phy_buf(self.mem)
		self.free_list = [(0, self.mem.nbytes)]
		self.malloced_list = []
	
	def malloc(self, nbytes, dtype=np.uint8, shape=(-1,)):
		result = None
		for i, (addr, size) in enumerate(self.free_list):
			if size >= nbytes:
				result = self.mem[addr:addr+nbytes]
				self.malloced_list.append((addr, nbytes))
				if size == nbytes:
					del self.free_list[i]
				else:
					# move base address by nbytes
					newbase = self.free_list[i][0] + nbytes
					newsize = self.free_list[i][1] - nbytes
					self.free_list[i] = (newbase, newsize)
				break

		if result != None:
			result.dtype = dtype
			result.shape = shape
			return result
		else:
			print "========================"
			print "ERROR: cannot allocate ", nbytes, "bytes"
			print "========================"
			return None

	def free(self, data):
		base, nbytes = self.mem_uio.phy_buf(data)
		base -= self.phy_base

		for i, (addr, size) in enumerate(self.malloced_list):
			if addr == base and size == nbytes:
				self.add_free(self.malloced_list[i])
				del self.malloced_list[i]
				return

		print "======================"
		print "ERROR: cannot free ", base, nbytes
		print "======================"

	def free_all(self):
		self.malloced_list = []
		self.free_list = [(0, self.mem.nbytes)]

	def add_free(self, data):
		base, nbytes = data
		for i, (addr, size) in enumerate(self.free_list):
			if addr > base:
				if base + nbytes == addr:
					# merge with next
					newbase = self.free_list[i][0] - nbytes
					newsize = self.free_list[i][1] + nbytes
					self.free_list[i] = (newbase, newsize)
				else:
					self.free_list.insert(i,(base, nbytes))

				if i != 0 and self.free_list[i-1][0] + self.free_list[i-1][1] == base:
					newbase = self.free_list[i][0] - self.free_list[i-1][1]
					newsize = self.free_list[i][1] + self.free_list[i-1][1]
					self.free_list[i] = (newbase, newsize)
					del self.free_list[i-1]
				return
		self.free_list.append((base, nbytes))
		if len(self.free_list.size) > 1 and self.free_list[-2][0] + self.free_list[-2][1] == base:
			newbase = self.free_list[-1][0] - self.free_list[-2][1]
			newsize = self.free_list[-1][1] + self.free_list[-2][1]
			self.free_list[-1] = (newbase, newsize)
			del self.free_list[-2]

	def print_free(self):
		print "free memory blocks:"
		for addr, size  in self.free_list:
			print "addr: {}\tsize: {}".format(addr, size)

	def print_malloced(self):
		print "allocated memory blocks:"
		for addr, size  in self.malloced_list:
			print "addr: {}\tsize: {}".format(addr, size)		

	@property
	def st(self):
		print "mm2s: 0x%x"%(self.dma.mm2s.st), "0x%x"%(self.dma.mm2s.length), "0x%x"%(self.dma.mm2s.cr)
		print "s2mm: 0x%x"%(self.dma.s2mm.st), "0x%x"%(self.dma.s2mm.length), "0x%x"%(self.dma.s2mm.cr)

	def toFPGA(self, a):
		self.dma.launch_mm2s(self.mem_uio.phy_buf(a))

	def toHost(self, a):
		self.dma.launch_s2mm(self.mem_uio.phy_buf(a))

	def waitForInterrupt(self, reset=False):
		return self.dma._dma_uio.wait_for_interrupt(reset)
		# return self.dma.wait()

	def wait(self):
		return self.dma.wait()

	def waitForIdle(self):
		while 1:
			if (self.dma.idle()):
				break

	def waitForToFPGA(self):
		while 1:
			if self.dma.mm2sidle():
				break
			# self.st
			# time.sleep(1)

	def waitForToHost(self):
		while 1:
			if self.dma.s2mmidle():
				break

def submit(device, p57, p125):
	# type check of argument p57
	assert type(p57) is np.ndarray
	p57_base, p57_size = device.mem_uio.phy_buf(p57)
	assert p57_base != None
	assert p57_size == 8192
	assert p57.dtype == np.int64
	assert p57.shape == (8, 128, )

	# type check of argument p125
	assert type(p125) is np.ndarray
	p125_base, p125_size = device.mem_uio.phy_buf(p125)
	assert p125_base != None
	assert p125_size == 131072
	assert p125.dtype == np.int64
	assert p125.shape == (128, 128, )

	# allocate memory for result and read from device
	t0 = device.malloc(8192, np.int64)
	device.toHost(t0)

	# transpose
	temp_1 = device.malloc(p125.nbytes, p125.dtype)
	temp_1.shape = (p125.shape[1], p125.shape[0]) + p125.shape[2:]
	temp_1[:] = np.transpose(p125,[1,0] + range(2, p125.ndim))
	# flatten
	# write data to device
	device.toFPGA(temp_1)
	device.waitForToFPGA()
	# flatten
	# write data to device
	device.toFPGA(p57)
	t0.shape = (8, 128)
	device.waitForIdle()
	return t0